<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Php;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function welcome(Request $request){
        // dd($request->all());
        $nama_awal = $request->Fnama;
        $nama_akhir = $request->Lnama;

        return view('halaman.welcome', compact('nama_awal','nama_akhir'));
    }
}
