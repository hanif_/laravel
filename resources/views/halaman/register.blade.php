@extends('layout.master')

@section('judul')
Halaman Input
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="Fnama"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="Lnama"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">other <br><br>
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa"> English <br>
        <input type="checkbox" name="bahasa"> Other <br><br>
        <label>Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
